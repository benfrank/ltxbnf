#!/ usr/bin/env texlua
-- Build script for ltxbnf

module = "ltxbnf"
stdengine = "luatex"
unpackfiles = {"*.dtx "}
typesetexe = "lualatex"
typesetopts = "--shell-escape"
checkopts = ""
