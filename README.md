# The `ltxbnf` package

## Project description

This package aims to help LaTeX users to typeset Backus-Naur-Forms, Extended-Backus-Naur-Forms and Augmented-Backus-Naur-Forms. It shall serve as an replacement for existing packages and does not focus on any compatibility to current solutions.

## Roadmap

You'll find the roadmap and known issues within the documentation.
